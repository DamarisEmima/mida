package integration;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import repository.LinkedTaskList;
import services.TaskIOService;
import stubs.TaskStub;

public class StubRepositoryServiceTest {

    private LinkedTaskList repo;
    private TaskIOService service;

    @BeforeEach
    void setUp(){

        MockitoAnnotations.initMocks(StubRepositoryServiceTest.class);
        repo = new LinkedTaskList();
        service = new TaskIOService();
        service.setLinkedTaskRepository(repo);
    }

    @AfterEach
    void tearDownEach(){

        repo = null;
        service = null;
    }

    @Test
    public void addTaskStub_valid() throws IllegalArgumentException {
        TaskStub taskStub = TaskStub.getStub();
        Assertions.assertEquals(service.getAllTasks().size(), 0);
        repo.add(taskStub);
        Assertions.assertEquals(service.getAllTasks().size(), 1);
    }

    @Test
    public void addTaskStub_invalid() throws IllegalArgumentException {

        TaskStub taskStub_invalid = TaskStub.getStubInvalid();
        Assertions.assertEquals(service.getAllTasks().size(), 0);
        Assertions.assertThrows(IllegalArgumentException.class,()->repo.add(taskStub_invalid));
        Assertions.assertEquals(service.getAllTasks().size(), 0);
    }
}
