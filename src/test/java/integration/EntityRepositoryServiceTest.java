package integration;

import model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repository.LinkedTaskList;
import services.TaskIOService;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EntityRepositoryServiceTest{

    private Task task;
    private LinkedTaskList repo;
    private TaskIOService service;
    private Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
    private Date end_valid = new GregorianCalendar(2020, Calendar.MARCH,15,12,0).getTime();

    @BeforeEach
    void setUp(){

        repo = new LinkedTaskList();
        service = new TaskIOService();
        service.setLinkedTaskRepository(repo);
        task = new Task("Title1",start_valid,end_valid,5);
    }

    @AfterEach
    void tearDownEach()  {
        repo = null;
        service = null;
        task = null;
    }

    @Test
    public void addTask_invalid(){
        task.setTitle(""); //invalid title
        Assertions.assertEquals(service.getAllTasks().size(), 0);
        Assertions.assertThrows(IllegalArgumentException.class, () -> repo.add(task));
        Assertions.assertEquals(service.getAllTasks().size(), 0);
    }

    @Test
    public void addTask_valid(){
        Assertions.assertEquals(service.getAllTasks().size(), 0);
        repo.add(task);
        Assertions.assertEquals(service.getAllTasks().size(), 1);
    }
}
