package services;

import model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import repository.LinkedTaskList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.Before;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.*;


import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

class TaskIOServiceTest {

    @Mock
    private LinkedTaskList repo;

    @InjectMocks
    private TaskIOService taskService;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
//        String title = "Task";
//        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
//        Task task = new Task(title, start);
//        ObservableList<Task> taskList = FXCollections.observableArrayList(Arrays.asList(task));
//        PowerMockito.mockStatic(TaskService.class);
//        PowerMockito.doNothing().when(TaskService.class, "rewriteFile", taskList);
    }

    @Test
    public void testTaskServiceAdd() throws Exception {

        String title = "Task";
        Date start = new GregorianCalendar(2020, Calendar.APRIL, 1, 20, 10).getTime();
        Task task = new Task(title, start);
        ObservableList<Task> taskList = FXCollections.observableArrayList(Collections.singletonList(task));
        //TaskService mock = Mockito.spy(new TaskService());
//        File file = createMock(File.class);
//        FileOutputStream fileOutputStream = createMock(FileOutputStream.class, file);
//        PowerMock.expectNew(FileOutputStream.class, file).andReturn(fileOutputStream);

        //PowerMockito.doNothing().when(mock, "writeBinary", Mockito.any(LinkedTaskRepository.class), file);
        Mockito.doNothing().when(taskService).rewriteFile(taskList);
        
        taskService.rewriteFile(taskList);

    }
}