package service;

import javafx.collections.FXCollections;
import model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import services.TasksOperations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {

    private TasksOperations tasksOperations;
    private TasksOperations tasksOperationsNull;
    private ArrayList<Task> tasks;

    private Date perioada_start = new GregorianCalendar(2020, Calendar.JANUARY,13,12,0).getTime();
    private Date perioada_final = new GregorianCalendar(2020, Calendar.APRIL,16,12,0).getTime();

    @BeforeEach
    void setUp() {
        Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
        Date end_valid = new GregorianCalendar(2020, Calendar.MARCH,15,12,0).getTime();
        Date start_valid2 = new GregorianCalendar(2020, Calendar.FEBRUARY,15,12,0).getTime();
        Date end_valid2 = new GregorianCalendar(2020, Calendar.APRIL,15,12,0).getTime();



        tasks=new ArrayList<>();
        Task task = new Task("Title1",start_valid,end_valid,5);
        task.setActive(true);
        tasks.add(task);
        Task task2 = new Task("Title2",start_valid2,end_valid2,60);
        task2.setActive(true);
        tasks.add(task2);

        tasksOperations=new TasksOperations(FXCollections.observableArrayList(tasks));
        tasksOperationsNull=new TasksOperations(FXCollections.observableArrayList(new ArrayList<>()));

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @Tag("development")
    void tc1_valid() {
        //afisarea task-urilor dintr-o perioada de timp valida
        // data de inceput si de final valida

        List<Task> allTask = new ArrayList<Task>();
        tasksOperations.incoming(perioada_start,perioada_final).forEach(allTask::add);
        assertEquals(2,allTask.size() );
    }

    @Test
    @Tag("development")
    void tc2_nonValid() {
        //afisarea task-urilor dintr-o perioada de timp non-valida
        // data de inceput non-valida start=null

        assertThrows(Exception.class,()-> tasksOperations.incoming(null,perioada_final) );
    }

    @Test
    @Tag("development")
    void tc3_nonValid(){
        //afisarea task-urilor dintr-o perioada de timp non-valida
        // data de sfarsit non-valida end=null

        assertThrows(Exception.class,()-> tasksOperations.incoming(perioada_start,null) );
    }

    @Test
    @Tag("development")
    void tc4_nonValid(){

        //incercarea de afisare a task-urilor dintr-o lista vida

        List<Task> allTask = new ArrayList<Task>();
        tasksOperationsNull.incoming(perioada_start,perioada_final).forEach(allTask::add);
        assertEquals(0,allTask.size() );
    }


}