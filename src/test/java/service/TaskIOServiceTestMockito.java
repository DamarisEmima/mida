package service;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Task;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.LinkedTaskList;
import services.TaskIOService;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TaskIOServiceTestMockito {

    @Mock
    private LinkedTaskList linkedTaskRepository;

    @Spy
    @InjectMocks
    private TaskIOService taskService = new TaskIOService();

    private Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();

    @Before
    public void setUp(){
    }

    @Test
    public void testAddTaskService_valid(){

        String title = "Task";
        Task task = new Task(title, start_valid);
        ObservableList<Task> taskList = FXCollections.observableArrayList(Collections.singletonList(task));
        Mockito.verify(linkedTaskRepository,Mockito.times(0)).add(task);
        TaskIOService.setSavedTasksFile(new File("classpath:tasks.txt"));
        taskService.rewriteFile(taskList);
        Mockito.verify(linkedTaskRepository,Mockito.times(1)).add(task);

    }

    @Test
    public void testAddTaskService_invalid() throws Exception {
        //invalid

        String title = "";
        Task task = new Task(title, start_valid);
        ObservableList<Task> taskList = FXCollections.observableArrayList(Collections.singletonList(task));
        Mockito.verify(linkedTaskRepository,Mockito.times(0)).add(task);
        TaskIOService.setSavedTasksFile(new File("classpath:tasks.txt"));

        try{
            taskService.rewriteFile(taskList);
        }catch (IllegalArgumentException e){
            assertEquals(e.getMessage(),"Title cannot be empty");
        }
        Mockito.verify(linkedTaskRepository,Mockito.times(0)).add(task);

    }

}