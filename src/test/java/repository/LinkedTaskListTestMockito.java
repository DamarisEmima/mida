package repository;

import model.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@MockitoSettings(strictness = Strictness.LENIENT)
public class LinkedTaskListTestMockito {

    @Mock
    private LinkedTaskList linkedTaskRepository;

    @Test
    public void testAddTask_valid() {

        String title = "Title1";
        Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
        Task task = new Task(title, start_valid);
        Mockito.doNothing().when(linkedTaskRepository).add(task);
        Mockito.when(linkedTaskRepository.size()).thenReturn(1);
        Assertions.assertEquals(linkedTaskRepository.size(), 1);
    }

    @Test
    public void testAddTask_invalid() {

        String title = "";
        Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
        Task task = new Task(title, start_valid);
        Mockito.doThrow(IllegalArgumentException.class).when(linkedTaskRepository).add(task);
        Assertions.assertEquals(linkedTaskRepository.size(), 0);
    }
}
