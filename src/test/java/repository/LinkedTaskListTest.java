package repository;

import model.Task;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;


//repository.repository.LinkedTaskListTest#tc3_...
class LinkedTaskListTest {

    private LinkedTaskList linkedTaskList;

    private Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
    private Date start_nonValid = new GregorianCalendar(-1, Calendar.JANUARY,-1,-1,-1).getTime();
    private Date end_nonValid = new GregorianCalendar(-1, Calendar.MARCH,-1,-1,-1).getTime();
    private Date end_valid = new GregorianCalendar(2020, Calendar.MARCH,15,12,0).getTime();


    @BeforeEach
    void setUp() {

        linkedTaskList = new LinkedTaskList();
    }

    @Test
//    @Tag("development")
    void tc3_ECP_valid() {
        //adaugarea unui Task cu data de finala valida

        Task task = new Task("Title1",start_valid,end_valid,5);
        linkedTaskList.add(task);
        assertEquals(1, linkedTaskList.size());
    }

    @Test
//    @Tag("development")
    void tc4_ECP_nonValid(){
        //adaugarea unui Task cu data de final non-valida

        try{
            linkedTaskList.add(new Task("Title1",start_valid,end_nonValid,5));
        }catch (IllegalArgumentException e){
            assertEquals("Time cannot be negative",e.getMessage());
        }
    }

    @Test
//    @DisplayName("ECP valid : for title")
    void tc5_ECP_valid(){
        //adaugarea unui Task cu title valid (lungimea string-ului > 0)

        Task task = new Task("Title1",start_valid);
        linkedTaskList.add(task);
        assertEquals(1,linkedTaskList.size());
    }

    @Test
    void tc6_ECP_nonValid(){
        //adaugarea unui Task cu title non-valid (lungimea string-ului =< 0)

        try{
            linkedTaskList.add(new Task("",start_valid));
        }catch (IllegalArgumentException e){
            assertEquals("Title cannot be empty",e.getMessage());
        }
    }

    @Test
    void tc1_BVA_valid(){
        //adaugarea unui Task cu interval valid (interval >= 1)

        Task task = new Task("Title1",start_valid,end_valid,5);
        linkedTaskList.add(task);
        assertEquals(1, linkedTaskList.size());
    }

//    @EnabledOnJre(JAVA_13)
    @Test
    void tc2_BVA_nonValid(){
        //adaugarea unui Task cu interval non-valid (interval < 1)

        try{
            linkedTaskList.add(new Task("Title1",start_valid,end_valid,0));
        }catch (IllegalArgumentException e){
            assertEquals("interval should me > 1",e.getMessage());
        }
    }

    @Test
//    @EnabledOnOs(WINDOWS)
    void tc7_BVA_valid(){
        //adaugarea unui Task cu data de start valida ( start.getTime() > 0 )

        Task task = new Task("Title1",start_valid,end_valid,1);
        linkedTaskList.add(task);
        assertEquals(1, linkedTaskList.size());
    }

    @Test
    void tc8_BVA_nonValid(){
        //adaugarea unui Task cu data de start non-valida ( start.getTime() < 0 )

        try{
            linkedTaskList.add(new Task("Title1",start_nonValid,end_valid,1));
        }catch (IllegalArgumentException e){
            assertEquals("Time cannot be negative",e.getMessage());
        }
    }
}