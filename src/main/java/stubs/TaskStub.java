package stubs;

import model.Task;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TaskStub extends Task {

    TaskStub(String title, Date start, Date end, int interval){
        super(title,start,end,interval);
    }

    private String title = "Title1";
    public static TaskStub getStub(){
        Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
        Date end_valid = new GregorianCalendar(2020, Calendar.MARCH,15,12,0).getTime();
        return new TaskStub("Title",start_valid,end_valid,5);
    }

    public static TaskStub getStubInvalid(){
        Date start_valid = new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
        Date end_valid = new GregorianCalendar(2020, Calendar.MARCH,15,12,0).getTime();
        return new TaskStub("",start_valid,end_valid,5);
    }

    public String getTitle(){
        return super.getTitle();
    }

    public Date getStart(){
        return new GregorianCalendar(2020, Calendar.JANUARY,15,12,0).getTime();
    }

    public Date getEnd(){
        return new GregorianCalendar(2020, Calendar.MARCH,15,12,0).getTime();
    }
    
    public int getInterval(){
        return 5;
    }




}
